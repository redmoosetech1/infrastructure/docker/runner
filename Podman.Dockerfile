FROM quay.io/podman/stable
LABEL version="${BUILD_VERSION}"
LABEL maintainer="RedMooseTech nate@redmoose.tech"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.name="RMT Gitlab Runner"
LABEL org.label-schema.description="RMT Gitlab Runner"
LABEL org.label-schema.url="https://redmoose.tech/"
LABEL org.label-schema.vcs-url="https://gitlab.com/redmoosetech1/infrastructure/docker/runner"
LABEL org.label-schema.vendor="RedMooseTech"
LABEL org.label-schema.version=${CI_COMMIT_BRANCH}
LABEL org.label-schema.schema-version="1.0"
LABEL com.microscaling.docker.dockerfile="/Podman.Dockerfile"

# Configure Internal Mirror
#RUN /bin/sed -i 's/https:\/\/dl-cdn.alpinelinux.org/http:\/\/mirror.rmt/g' /etc/apk/repositories

# Add Base Packages
RUN dnf -y update
RUN sudo dnf -y groupinstall "Development Tools"
RUN dnf install -y dnf-plugins-core
RUN dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo
RUN dnf -y install \
    curl \
    bash \
    python3 \
    python3-pip \
    python3-virtualenv \
    python3-wheel \
    python3-devel \
    libffi-devel \
    openssl-devel \
    make \
    git \
    grep \
    bind-utils \
    glibc-devel \
    ca-certificates \
    jq \
    libssh-devel \
    yum-utils \
    terraform \
    vault

# ADD https://mirror.rmt/certificates/rmt.crt /usr/local/share/ca-certificates/rmt.crt
RUN update-ca-trust

ENV PATH "/root/.local/bin:/root/.bin:$PATH"
RUN echo $PATH

RUN /usr/bin/python3 -m pip install 'tox >= 3.19.0' --user
RUN /usr/bin/python3 -m pip install \
    --user \
    --no-binary ansible-pylibssh \
    ansible-pylibssh
RUN /usr/bin/python3 -m pip install --upgrade pip && \
    python3 -m pip install \
    setuptools_rust \
    docker-compose \
    yamllint \
    ansible \
    python-gitlab \
    netaddr \
    hvac \
    capturer \
    cement \
    coverage \
    numpy \
    pytest \
    pytest-cov \
    --user --no-binary ansible-pylibssh

# Install Ansible Galaxy Packages
# This takes a while will time out if we install all on one line.
RUN ansible-galaxy collection install community.vmware
RUN ansible-galaxy collection install community.crypto \
community.docker \
community.hashi_vault \
ansible.posix \
community.general

# Install dependencies for Ansible Galaxy
RUN /usr/bin/python3 -m pip install -r ~/.ansible/collections/ansible_collections/community/vmware/requirements.txt --user

RUN mkdir -p ~/.docker
