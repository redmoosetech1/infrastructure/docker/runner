FROM docker:dind
LABEL version="${BUILD_VERSION}"
LABEL maintainer="RedMooseTech nate@redmoose.tech"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.name="RMT Gitlab Runner"
LABEL org.label-schema.description="RMT Gitlab Runner"
LABEL org.label-schema.url="https://redmoose.tech/"
LABEL org.label-schema.vcs-url="https://gitlab.com/infrastructure/docker/runner"
LABEL org.label-schema.vendor="RedMooseTech"
LABEL org.label-schema.version=${CI_COMMIT_BRANCH}
LABEL org.label-schema.schema-version="1.0"
LABEL com.microscaling.docker.dockerfile="/Dockerfile"

# Configure Internal Mirror
#RUN /bin/sed -i 's/https:\/\/dl-cdn.alpinelinux.org/http:\/\/mirror.rmt/g' /etc/apk/repositories

# Add Base Packages
RUN apk update && \
    apk add --no-cache \
    curl \
    bash \
    python3-dev \
    py3-pip \
    py3-virtualenv \
    py3-wheel \
    libffi-dev \
    openssl-dev \
    gcc \
    make \
    git \
    grep \
    bind-tools \
    py-pip \
    libc-dev \
    ca-certificates \
    jq \
    libssh-dev \
    openssl-dev \
    alpine-sdk \
    build-base \
    vault

# ADD https://mirror.rmt/certificates/rmt.crt /usr/local/share/ca-certificates/rmt.crt
# RUN update-ca-certificates

# Install Terraform
# RUN wget https://mirror.rmt/binaries/amd64/terraform_1.1.6_linux_amd64.zip
# RUN unzip terraform_1.1.6_linux_amd64.zip && rm terraform_1.1.6_linux_amd64.zip
# RUN mv terraform /usr/bin/terraform
RUN apk add terraform --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community

ENV PATH "/root/.local/bin:/root/.bin:$PATH"
RUN echo $PATH

RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install \
    setuptools_rust \
    docker-compose \
    yamllint \
    ansible \
    ansible-pylibssh \
    python-gitlab \
    netaddr \
    hvac \
    capturer \
    cement \
    coverage \
    numpy \
    pytest \
    pytest-cov \
    --user

# Install Ansible Galaxy Packages
# This takes a while will time out if we install all on one line.
RUN ansible-galaxy collection install community.vmware
RUN ansible-galaxy collection install community.crypto \
community.docker \
community.hashi_vault \
ansible.posix \
community.general

# Install dependencies for Ansible Galaxy
RUN python3 -m pip install -r ~/.ansible/collections/ansible_collections/community/vmware/requirements.txt --user

RUN mkdir -p ~/.docker
