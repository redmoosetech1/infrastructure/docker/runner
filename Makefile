GIT_SHA1                 := $(shell git rev-parse --short HEAD)
export VCS_REF           := $(GIT_SHA1)
export BUILD_DATE        := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ')
export BUILD_VERSION     := $(RUNNER_VERSION)
export BUILD_TYPE        = stable
export RUNNER_VERSION    = main
export RUNNER_IMAGE_NAME = redmoosetech/rmt-runner

default: build

build:
	docker build \
	-t $(RUNNER_IMAGE_NAME):$(RUNNER_VERSION) \
	-t $(RUNNER_IMAGE_NAME):latest \
	.
	docker push $(RUNNER_IMAGE_NAME):$(RUNNER_VERSION)
	docker push $(RUNNER_IMAGE_NAME):latest

